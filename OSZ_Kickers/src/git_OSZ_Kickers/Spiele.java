package git_OSZ_Kickers;

public class Spiele {

private String heimmannschaft;
private String gastmannschaft;
private String zeitpunkt;
private String endstand;
private int gelbeKarten;
private int roteKarten;


public Spiele(){
}

public void setHeimmannschaft(String heimmannschaft){
this.heimmannschaft = heimmannschaft;	
}
public String getHeimmannschaft(){
return heimmannschaft;
}
public void setGastmannschaft(String gastmannschaft){
this.gastmannschaft = gastmannschaft;	
}
public String getGastmannschaft(){
return gastmannschaft;
}
public void setZeitpunkt(String zeitpunkt){
this.zeitpunkt = zeitpunkt;
}
public String getZeitpunkt(){
return zeitpunkt;
}
public void setEndstand(String endstand){
this.endstand = endstand;
}
public String getEndstand(){
return endstand;
}
public void setGelbeKarten(int gelbeKarten){
this.gelbeKarten = gelbeKarten;
}
public int getGelbeKarten(){
return gelbeKarten;
}
public void setRoteKarten(int roteKarten){
this.roteKarten = roteKarten;
}
public int getRoteKarten(){
return roteKarten;
}

}
