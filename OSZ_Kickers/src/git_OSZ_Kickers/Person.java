package git_OSZ_Kickers;

public abstract class Person {

private String name;
private String telefonnummer;
private boolean jahresbeitrag;


public Person(){ 
}

public void setName (String name){
	this.name = name;
}
public String getName (){
	return name;
}
public void setTelefonnummer (String telefonnummer){
	this.telefonnummer = telefonnummer;
}
public String getTelefonnummer (){
	return telefonnummer;
}
public void setJahresbeitrag (boolean jahresbeitrag){
	this.jahresbeitrag = jahresbeitrag;
}
public boolean getJahresbeitrag (){
	 return jahresbeitrag;
}

}