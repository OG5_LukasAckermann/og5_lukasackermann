package git_OSZ_Kickers;

public class TestKickers {

public static void main(String[] args) {
		
Spieler s = new Spieler();
Trainer t = new Trainer();
Schiedsrichter sr = new Schiedsrichter();
Mannschaftsleiter ml = new Mannschaftsleiter();

s.setName("Cristiano Ronaldo");
s.setPosition("St�rmer");
s.setTrikotnummer(7);
s.setTelefonnummer("12345678910");
s.setJahresbeitrag(true);

t.setName("Zinedine Zidane");
t.setTelefonnummer("10987654321");
t.setLizenzklasse("A");
t.setAufwandentschaedigung(500);
t.setJahresbeitrag(true);

sr.setName("Carlos Carballo");
sr.setTelefonnummer("246810121416");
sr.setAnzahlGepfiffeneSpiele(100);
sr.setJahresbeitrag(true);

ml.setName("Roberto");
ml.setTelefonnummer("136912151821");
ml.setNameMannschaft("Real Madrid");
ml.setRabatt(25);

System.out.println("Spieler");
System.out.println(s.getName());
System.out.println(s.getTelefonnummer());
System.out.println(s.getPosition());
System.out.println(s.getTrikotnummer());
System.out.println(s.getJahresbeitrag());

System.out.println("");


System.out.println("Trainer");
System.out.println(t.getName());
System.out.println(t.getTelefonnummer());
System.out.println(t.getLizenzklasse());
System.out.println(t.getAufwandentschaedigung());
System.out.println(t.getJahresbeitrag());

System.out.println("");

System.out.println("Schiedsrichter");
System.out.println(sr.getName());
System.out.println(sr.getTelefonnummer());
System.out.println(sr.getAnzahlGepfiffeneSpiele());
System.out.println(sr.getJahresbeitrag());

System.out.println("");

System.out.println("Mannschaftsleiter");
System.out.println(ml.getName());
System.out.println(ml.getTelefonnummer());
System.out.println(ml.getNameMannschaft());
System.out.println(ml.getRabatt());

}

}
