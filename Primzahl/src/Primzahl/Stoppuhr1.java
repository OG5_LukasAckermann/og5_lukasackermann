package Primzahl;

public class Stoppuhr1
{ 

private long millis;


public void starte()
{ millis = System.currentTimeMillis();
}

public void stoppe()
{ millis = System.currentTimeMillis() - millis;
}

public long dauer()
{ return millis;
}

} 