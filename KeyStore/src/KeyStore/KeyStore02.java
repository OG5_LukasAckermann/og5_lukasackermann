package KeyStore;

import java.util.ArrayList;

public class KeyStore02 {

	private ArrayList<String> keylist;

	public KeyStore02() {
		keylist = new ArrayList<String>();
	}

	public void clear() {
		keylist.clear();
	}

	public int indexOf(String str) {
		return keylist.indexOf(str);
	}

	public String get(int index) {
		return keylist.get(index);
	}

	public int size() {
		return keylist.size();
	}

	public boolean add(String e) {
		return keylist.add(e);
	}

	public String toString() {
		return keylist.toString();
	}

	public boolean remove(String str) {
		return keylist.remove(str);
	}

	public boolean remove(int index) {
		if (keylist.remove(index) != null)
			return true;
		else
			return false;
	}

}
