package SelectSort;

public class SelectSort {

	public static void main(String[] args) {
		int[] array = {0, 2, 8, 4, 1, 6, 5, 3, 7, 9, 10};
		
		System.out.print("Unsortiert: ");
	
		for (int i = 0; i < array.length; i++){
            System.out.print(array[i] + " | ");
        }
        System.out.println("");
		Sort(array);

		System.out.print("Sortiert:   ");
        for (int i = 0; i < array.length; i++){
            System.out.print(array[i] + " | ");
        }
	}
	
	public static int[] Sort(int[] a) {
		for (int i = 0; i < a.length - 1; i++) {
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] > a[j]) {
					int h = a[i];
					a[i] = a[j];
					a[j] = h;
					System.out.print(a[i] + "--" + a[j] + " ------ ");
			        for (int n = 0; n < a.length; n++){
			            System.out.print(a[n]+" | ");
			        }
			        System.out.println();
				}
			}
		}

		return a;
	}
}