package Stoppuhr;

public class Stoppuhr {

private long startpunkt;
private long endpunkt;

public Stoppuhr(){
}

public void starten(){
startpunkt = System.currentTimeMillis();
}

public void stoppen(){
endpunkt = System.currentTimeMillis();
}

public void reseten(){
startpunkt = 0;
endpunkt = 0;
}

public long getDauer(){
return endpunkt - startpunkt;
}

}


