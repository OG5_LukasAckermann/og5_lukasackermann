package Voting;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;

public class Startseite extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Startseite frame = new Startseite();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Startseite() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblMusikvoting = new JLabel("Musikvoting");
		lblMusikvoting.setForeground(Color.WHITE);
		lblMusikvoting.setBackground(Color.WHITE);
		lblMusikvoting.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblMusikvoting.setBounds(156, 89, 124, 38);
		panel.add(lblMusikvoting);
		
		JButton btnAnmelden = new JButton("Anmelden");
		btnAnmelden.setBounds(110, 158, 101, 23);
		panel.add(btnAnmelden);
		btnAnmelden.addActionListener(new ActionListener() {
		     public void actionPerformed(ActionEvent e) {
		   
		     new Anmeldung();
		      }
		       });
	
		
		JButton btnRegistrieren = new JButton("Registrieren");
		btnRegistrieren.setBounds(238, 158, 101, 23);
		panel.add(btnRegistrieren);
		}
}