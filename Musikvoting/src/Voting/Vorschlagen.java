package Voting;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class Vorschlagen extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vorschlagen frame = new Vorschlagen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Vorschlagen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitelVorschlagen = new JLabel("Titel vorschlagen");
		lblTitelVorschlagen.setForeground(Color.WHITE);
		lblTitelVorschlagen.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblTitelVorschlagen.setBounds(129, 11, 179, 29);
		contentPane.add(lblTitelVorschlagen);
		
		textField = new JTextField();
		textField.setBounds(176, 80, 169, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(176, 111, 169, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(176, 142, 169, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblBandname = new JLabel("Bandname:");
		lblBandname.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblBandname.setForeground(Color.WHITE);
		lblBandname.setBounds(111, 82, 70, 14);
		contentPane.add(lblBandname);
		
		JLabel lblTitel = new JLabel("Titel:");
		lblTitel.setForeground(Color.WHITE);
		lblTitel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTitel.setBounds(144, 113, 46, 14);
		contentPane.add(lblTitel);
		
		JLabel lblGenre = new JLabel("Genre:");
		lblGenre.setForeground(Color.WHITE);
		lblGenre.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblGenre.setBounds(135, 144, 46, 14);
		contentPane.add(lblGenre);
		
		JButton btnHinzufgen = new JButton("Hinzuf\u00FCgen");
		btnHinzufgen.setBounds(98, 192, 124, 23);
		contentPane.add(btnHinzufgen);
		
		JButton btnZurcksetzen = new JButton("Zur\u00FCcksetzen");
		btnZurcksetzen.setBounds(234, 192, 124, 23);
		contentPane.add(btnZurcksetzen);
		
		JLabel lblSieKnnen = new JLabel("Sie k\u00F6nnen maximal 5 Titel vorschlagen.");
		lblSieKnnen.setHorizontalAlignment(SwingConstants.CENTER);
		lblSieKnnen.setForeground(Color.WHITE);
		lblSieKnnen.setBounds(97, 49, 248, 14);
		contentPane.add(lblSieKnnen);
	}

}
