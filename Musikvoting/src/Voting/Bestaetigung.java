package Voting;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;

public class Bestaetigung extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bestaetigung frame = new Bestaetigung();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Bestaetigung() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblIhrVotingOder = new JLabel("Ihr Voting oder Vorschlag \r\nwurde erfolgreich hinzugef\u00FCgt.");
		lblIhrVotingOder.setHorizontalAlignment(SwingConstants.TRAILING);
		lblIhrVotingOder.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblIhrVotingOder.setForeground(Color.WHITE);
		lblIhrVotingOder.setBounds(42, 25, 347, 113);
		contentPane.add(lblIhrVotingOder);
		
		JButton btnAccount = new JButton("Zu Account");
		btnAccount.setBounds(62, 135, 145, 43);
		contentPane.add(btnAccount);
		
		JButton btnAktuellesRanking = new JButton("Aktuelles Ranking");
		btnAktuellesRanking.setBounds(238, 135, 150, 43);
		contentPane.add(btnAktuellesRanking);
	}

}
