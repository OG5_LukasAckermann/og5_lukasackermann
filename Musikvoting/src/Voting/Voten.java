package Voting;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JScrollBar;

public class Voten extends JFrame {

	private JPanel contentPane;
	private JTable VotingTabelle;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Voten frame = new Voten();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Voten() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 524, 438);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitelVoten = new JLabel("Musikvoting");
		lblTitelVoten.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblTitelVoten.setForeground(Color.WHITE);
		lblTitelVoten.setBounds(203, 11, 124, 37);
		contentPane.add(lblTitelVoten);
		
		VotingTabelle = new JTable();
		VotingTabelle.setForeground(Color.WHITE);
		VotingTabelle.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
			},
			new String[] {
				"Rang", "Bandname", "Titel", "Genre", "Vote"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, String.class, String.class, Boolean.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		VotingTabelle.getColumnModel().getColumn(0).setPreferredWidth(36);
		VotingTabelle.getColumnModel().getColumn(2).setPreferredWidth(159);
		VotingTabelle.getColumnModel().getColumn(3).setPreferredWidth(73);
		VotingTabelle.getColumnModel().getColumn(4).setPreferredWidth(32);
		VotingTabelle.setBounds(59, 70, 413, 256);
		contentPane.add(VotingTabelle);
		
		JButton btnSenden = new JButton("Senden");
		btnSenden.setBounds(134, 351, 112, 37);
		contentPane.add(btnSenden);
		
		JButton btnZurcksetzen = new JButton("Zur\u00FCcksetzen");
		btnZurcksetzen.setBounds(276, 351, 112, 37);
		contentPane.add(btnZurcksetzen);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(455, 144, 17, 48);
		contentPane.add(scrollBar);
	}
}
