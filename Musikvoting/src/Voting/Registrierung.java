package Voting;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Registrierung extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Registrierung frame = new Registrierung();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Registrierung() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRegistrierung = new JLabel("Registrierung");
		lblRegistrierung.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblRegistrierung.setForeground(Color.WHITE);
		lblRegistrierung.setBackground(Color.WHITE);
		lblRegistrierung.setBounds(151, 24, 140, 29);
		contentPane.add(lblRegistrierung);
		
		textField = new JTextField();
		textField.setBounds(191, 85, 152, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(191, 116, 152, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(191, 147, 152, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnRegistrieren = new JButton("Registrieren");
		btnRegistrieren.setBounds(107, 193, 113, 23);
		contentPane.add(btnRegistrieren);
		
		JButton btnZurcksetzen = new JButton("Zur\u00FCcksetzen");
		btnZurcksetzen.setBounds(247, 193, 113, 23);
		contentPane.add(btnZurcksetzen);
		
		JLabel lblBandname = new JLabel("Benutzername:");
		lblBandname.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblBandname.setForeground(Color.WHITE);
		lblBandname.setBounds(107, 87, 103, 14);
		contentPane.add(lblBandname);
		
		JLabel lblTitel = new JLabel("Vorname:");
		lblTitel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTitel.setForeground(Color.WHITE);
		lblTitel.setBounds(135, 118, 64, 14);
		contentPane.add(lblTitel);
		
		JLabel lblGenre = new JLabel("Nachname:");
		lblGenre.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblGenre.setForeground(Color.WHITE);
		lblGenre.setBounds(123, 149, 86, 14);
		contentPane.add(lblGenre);
	}

}
