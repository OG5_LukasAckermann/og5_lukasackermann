package Voting;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Anmeldung extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Anmeldung frame = new Anmeldung();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Anmeldung() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAnmeldung = new JLabel("Anmeldung");
		lblAnmeldung.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblAnmeldung.setForeground(Color.WHITE);
		lblAnmeldung.setBounds(154, 11, 122, 46);
		contentPane.add(lblAnmeldung);
		
		textField = new JTextField();
		textField.setBounds(211, 116, 109, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblBenutzernae = new JLabel("Benutzername:");
		lblBenutzernae.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblBenutzernae.setForeground(Color.WHITE);
		lblBenutzernae.setBounds(117, 118, 91, 14);
		contentPane.add(lblBenutzernae);
		
		JButton btnAnmelden = new JButton("Anmelden");
		btnAnmelden.setBounds(119, 184, 89, 23);
		contentPane.add(btnAnmelden);
		
		JButton btnZurcksetzen = new JButton("Zur\u00FCcksetzen");
		btnZurcksetzen.setBounds(223, 184, 97, 23);
		contentPane.add(btnZurcksetzen);
	}
}
