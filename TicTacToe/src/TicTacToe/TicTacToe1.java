package TicTacToe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import java.awt.event.ActionEvent;

public class TicTacToe1 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TicTacToe1 frame = new TicTacToe1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TicTacToe1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(3, 3, 0, 0));
		
		JButton btnNewButton = new JButton("");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton.getText().equals("")){
					btnNewButton.setText("X");}
					else {
						if(btnNewButton.getText().equals("X"))
							btnNewButton.setText("O");
					}
			}
		});
		
		
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_2.getText().equals("")){
					btnNewButton_2.setText("X");}
					else {
						if(btnNewButton_2.getText().equals("X"))
							btnNewButton_2.setText("O");
					}
			}
		});
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_3.getText().equals("")){
					btnNewButton_3.setText("X");}
					else {
						if(btnNewButton_3.getText().equals("X"))
							btnNewButton_3.setText("O");
					}
			}
		});
		contentPane.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_4.getText().equals("")){
					btnNewButton_4.setText("X");}
					else {
						if(btnNewButton_4.getText().equals("X"))
							btnNewButton_4.setText("O");
					}
			}
		});
		contentPane.add(btnNewButton_4);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_1.getText().equals("")){
					btnNewButton_1.setText("X");}
					else {
						if(btnNewButton_1.getText().equals("X"))
							btnNewButton_1.setText("O");
					}
			}
		});
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_6 = new JButton("");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_6.getText().equals("")){
					btnNewButton_6.setText("X");}
					else {
						if(btnNewButton_6.getText().equals("X"))
							btnNewButton_6.setText("O");
					}
			}
		});
		contentPane.add(btnNewButton_6);
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_5.getText().equals("")){
					btnNewButton_5.setText("X");}
					else {
						if(btnNewButton_5.getText().equals("X"))
							btnNewButton_5.setText("O");
					}
			
			}
		});
		contentPane.add(btnNewButton_5);
		
		JButton btnNewButton_7 = new JButton("");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					if (btnNewButton_7.getText().equals("")){
						btnNewButton_7.setText("X");}
						else {
							if(btnNewButton_7.getText().equals("X"))
								btnNewButton_7.setText("O");
						}		
			}
		});
		contentPane.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnNewButton_8.getText().equals("")){
					btnNewButton_8.setText("X");}
					else {
						if(btnNewButton_8.getText().equals("X"))
							btnNewButton_8.setText("O");
					}
			}
		});
		contentPane.add(btnNewButton_8);
	}

}
