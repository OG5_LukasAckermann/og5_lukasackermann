/* Fibonacci.java
   Programm zum Testen der rekursiven und der iterativen Funktion
   zum Berechnen der Fibonacci-Zahlen.
   AUFGABE: Implementieren Sie die Methoden fiboRekursiv() und fiboIterativ()
   HINWEIS: siehe Informationsblatt "Fibonacci-Zahlen oder das Kaninchenproblem"
   Autor:
   Version: 1.0
   Datum:
*/
public class Fibonacci{
   // Konstruktor
   Fibonacci(){
   }
   
  /**
    * Rekursive Berechnung der Fibonacci-Zahl an n-te Stelle
    * @param n 
    * @return die n-te Fibonacci-Zahl
   */
  long fiboRekursiv(int n){
     if(n == 0){
     return 0;
     }
     if(n == 1){
    	 return 1;
     }
     return fiboRekursiv(n - 2) + fiboRekursiv(n - 1);
  }//fiboRekursiv

  /**
   * Iterative Berechnung der Fibonacci-Zahl an n-te Stelle
   * @param n 
   * @return die n-te Fibonacci-Zahl
  */
  long fiboIterativ(int n){
   
	int a = 0;
    int b = 1;
    int c = 0;
    
    for(int i = 0; i <= n; i++){
    	
    	a = a + c;
    	c = b;
    	b = a;
    }
      return a;
  }//fiboIterativ

}// Fibonnaci


