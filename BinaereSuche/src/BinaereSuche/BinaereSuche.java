package BinaereSuche;

import java.util.Scanner;

public class BinaereSuche {
	
	public static void main(String[] args)
	   {
		System.out.print("Gesuchte Zahl: ");
		
		  Scanner tastatur = new Scanner(System.in);
	      int a = tastatur.nextInt();
	      
	      int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	      
	      int obergrenze = array.length-1;
	      int untergrenze = 0;
	      
	      System.out.println(suche(untergrenze, a, obergrenze, array));
	   }

	static boolean suche(int untergrenze, int a, int obergrenze, int[] array)
	   {
	      int i = (untergrenze + obergrenze + 2) / 2;
	      if (array[i] == a || array[untergrenze] == a || array[obergrenze] == a)
	      {
	         return true;
	      }
	      else if (untergrenze >= obergrenze)
	      {
	         return false;
	      }
	      else
	      {
	         if (array[i] < a)
	         {
	            return suche(i, a, obergrenze - 1, array);
	         }
	         else
	         {
	            return suche(untergrenze + 1, a, i, array);
	         }
	      }
	   }
	}
	 