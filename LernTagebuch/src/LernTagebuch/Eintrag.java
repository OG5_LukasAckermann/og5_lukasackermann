package LernTagebuch;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.FlowLayout;
import java.awt.Insets;
import javax.swing.JScrollPane;

public class Eintrag extends JFrame implements ActionListener {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JButton btnSchliessen;

  
  public Eintrag(ActionListener actionListener) {
    super();
    setPreferredSize(new Dimension(400, 350));
    setMinimumSize(new Dimension(100, 100));
    setBounds(new Rectangle(100, 100, 100, 100));
    JPanel meinPanel = new JPanel();
    meinPanel.setLayout(new BorderLayout());
    
    JPanel panel = new JPanel();
	getContentPane().add(panel, BorderLayout.CENTER);
	panel.setLayout(new BorderLayout(0, 0));
	
	JPanel panel_1 = new JPanel();
	panel_1.setFont(new Font("Arial Black", Font.PLAIN, 18));
	panel_1.setMinimumSize(new Dimension(16, 16));
	panel_1.setSize(new Dimension(4, 4));
	panel.add(panel_1, BorderLayout.NORTH);
	
	JLabel lblBericht = new JLabel("Eintrag");
	lblBericht.setFont(new Font("Arial", Font.PLAIN, 18));
	panel_1.add(lblBericht);
	
	JPanel panel_2 = new JPanel();
	panel.add(panel_2, BorderLayout.SOUTH);
	
	JButton btnNew = new JButton("Hinzufügen");
	panel_2.add(btnNew);
	
	btnSchliessen = new JButton("Schlie\u00DFen");
	panel_2.add(btnSchliessen);
	btnSchliessen.addActionListener(this);
	
	
	
	JPanel panel_3 = new JPanel();
	panel.add(panel_3, BorderLayout.CENTER);
	panel_3.setLayout(new GridLayout(4, 2, 0, 0));
	
	JPanel panel_5 = new JPanel();
	panel_3.add(panel_5);
	panel_5.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
	
	JLabel lblNewLabel = new JLabel("Fach      ");
	lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	lblNewLabel.setHorizontalTextPosition(SwingConstants.CENTER);
	panel_5.add(lblNewLabel);
	
	JTextArea textArea_3 = new JTextArea();
	textArea_3.setMargin(new Insets(2, 2, 2, 310));
	panel_5.add(textArea_3);
	
	JPanel panel_6 = new JPanel();
	panel_3.add(panel_6);
	panel_6.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
	
	JLabel lblNewLabel_1 = new JLabel("Datum   ");
	lblNewLabel_1.setHorizontalTextPosition(SwingConstants.CENTER);
	lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
	panel_6.add(lblNewLabel_1);
	
	JTextArea textArea_2 = new JTextArea();
	textArea_2.setMargin(new Insets(2, 2, 2, 310));
	panel_6.add(textArea_2);
	
	JPanel panel_7 = new JPanel();
	panel_3.add(panel_7);
	panel_7.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
	
	JLabel lblNewLabel_2 = new JLabel("Dauer    ");
	lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
	lblNewLabel_2.setHorizontalTextPosition(SwingConstants.CENTER);
	panel_7.add(lblNewLabel_2);
	
	JTextArea textArea_1 = new JTextArea();
	textArea_1.setMargin(new Insets(2, 2, 2, 310));
	panel_7.add(textArea_1);
	
	JPanel panel_4 = new JPanel();
	panel_3.add(panel_4);
	panel_4.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
	
	JLabel lblNewLabel_3 = new JLabel("Aktivit\u00E4t");
	lblNewLabel_3.setHorizontalTextPosition(SwingConstants.CENTER);
	lblNewLabel_3.setHorizontalAlignment(SwingConstants.RIGHT);
	panel_4.add(lblNewLabel_3);
	
	JTextArea textArea = new JTextArea();
	textArea.setMargin(new Insets(2, 2, 2, 310));
	panel_4.add(textArea);
	
    this.pack();
    this.setVisible(true);
  }


@Override
public void actionPerformed(ActionEvent e) {
	if(e.getSource() == btnSchliessen){
		this.dispose();
	
}
}
}

   
  
