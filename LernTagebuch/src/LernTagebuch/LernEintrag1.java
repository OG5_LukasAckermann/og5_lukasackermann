package LernTagebuch;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import javax.swing.JTextArea;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JScrollPane;
import java.awt.Font;
import javax.swing.SwingConstants;
public class LernEintrag1 extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JButton btnNewButton_2;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		new LernEintrag1();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LernEintrag1 frame = new LernEintrag1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	
	
	public LernEintrag1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		Panel panel = new Panel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		
		JButton btnNewButton_1 = new JButton("Neuer Eintrag...");
		panel.add(btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			   if (e.getSource() == btnNewButton_1) {
				        new Eintrag(this);
			   }
	    }
	});
		
		JButton btnNewButton_2 = new JButton("Bericht...");
		panel.add(btnNewButton_2);
		btnNewButton_2.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			   if (e.getSource() == btnNewButton_2) {
				        new Report(this);
			   }
	    }
	});
		
		JButton btnNewButton = new JButton("Beenden");
		btnNewButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		        System.exit(0);
		    }
		});
		
		panel.add(btnNewButton);
		
		Panel panel_1 = new Panel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
		
		JLabel lblNewLabel = new JLabel("                             Lerntagebuch von Lukas");
		lblNewLabel.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 16));
		panel_1.add(lblNewLabel);
		
		JTextArea textArea = new JTextArea();
		contentPane.add(textArea, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.EAST);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
		
		    
		  }

		
	



