package LernTagebuch;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import javax.swing.JScrollPane;

public class Report extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JButton btnSchliessen;

  
  public Report(ActionListener actionListener) {
    super();
    setMinimumSize(new Dimension(400, 350));
    JPanel meinPanel = new JPanel();
    meinPanel.setLayout(new BorderLayout());
    
    JPanel panel = new JPanel();
	getContentPane().add(panel, BorderLayout.CENTER);
	panel.setLayout(new BorderLayout(0, 0));
	
	JPanel panel_1 = new JPanel();
	panel.add(panel_1, BorderLayout.NORTH);
	
	JLabel lblBericht = new JLabel("Bericht");
	lblBericht.setFont(new Font("Arial", Font.PLAIN, 18));
	panel_1.add(lblBericht);
	
	JPanel panel_2 = new JPanel();
	panel.add(panel_2, BorderLayout.SOUTH);
	
	btnSchliessen = new JButton("Schlie�en");
	panel_2.add(btnSchliessen);
	btnSchliessen.addActionListener(this);
	
	JTextArea textArea = new JTextArea();
	panel.add(textArea, BorderLayout.CENTER);
	
	JScrollPane scrollPane = new JScrollPane();
	panel.add(scrollPane, BorderLayout.EAST);
	scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    

    this.pack();
    this.setVisible(true);
  }


@Override
public void actionPerformed(ActionEvent e) {
	if(e.getSource() == btnSchliessen){
		this.dispose();
	}
	
}
}

   
  
