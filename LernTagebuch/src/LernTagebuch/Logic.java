package LernTagebuch;

import java.util.List;

public class Logic {

	private List<Lerneintrag> listEntries;
	private String learnerName;
	private final FileControl FILE_CONTROL;

	public Logic(FileControl fc) {
		this.FILE_CONTROL = fc;
		reloadFromDisk();
	}

	public void reloadFromDisk() {
		LearnerData ldata = FILE_CONTROL.getFileContent();
		this.learnerName = ldata.getLearnerName();
		this.listEntries = ldata.getListEntries();
	}

	public List<Lerneintrag> getListEntries() {
		return listEntries;
	}

	public void setListEntries(List<Lerneintrag> listEntries) {
		this.listEntries = listEntries;
	}

	public String getLearnerName() {
		return learnerName;
	}

	public boolean addEntry(Lerneintrag l) {
		if (FILE_CONTROL.addEntry(l)) {
			listEntries.add(l);
			return true;
		}
		return false;
	}

	public String createReport() {
		return "";
	}

}
