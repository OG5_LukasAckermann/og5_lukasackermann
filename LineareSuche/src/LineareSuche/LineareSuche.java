package LineareSuche;

import java.util.Scanner;

public class LineareSuche {

	public static int lineareSuche(int gesucht, int array[]) {
		
		int i = 0;
		
		while (i < array.length) {
			if (array[i] == gesucht) {
				return i;			
			}
			i++;
		}	
		return -1;		
	}

	public static void main(String[] args) {

		int[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Gesuchte Zahl: ");
		int zahl = scan.nextInt();

		System.out.println("Position: " + lineareSuche(zahl, a));
		
	}

}